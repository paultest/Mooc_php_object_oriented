<?php

class MagicTest{
	//__tostring会在把对象转换为string的时候自动调用
	public function __tostring(){
		return "这个是MagicTest类的tostring方法\n";
	}
	//__invoke会在把对象当做一个方法调用的时候自动调用
	public function __invoke($x){
		echo "调用了MagicTest类的invoke方法，参数为：".$x."\n";
	}

	public function aaa(){
		return "这个是MagicTest类的aaa方法\n";
	}

}

$obj = new MagicTest();
echo $obj;
$obj(888);

?>
