<?php

//类的定义以关键字class开始，后面跟着这个类的名称。类的明明通常每个单词的第一个字母大写。以中括号开始和结束
class NbaPlayer{
	public $name = "Jordan";  //定义属性
	public $height = "198cm";
	public $weight = "98kg";
	public $team = "Bull";
	public $palyerNumber = "23";

	//构造函数，在对象被实例化的时候自动调用，即下面的$jorder = new NbaPlayer;就调用了构造函数	
	function __construct(){
		echo "In NbaPlayer constructor\n";
	}

	//定义方法
	public function run(){
		echo "Running\n";
	}
	
	public function jump(){
		echo "Jumping\n";
	}

	public function dribble(){
		echo "Dribbling\n";
	}

	public function shoot(){
		echo "Shooting\n";
	}

	public function dunk(){
		echo "Dunking\n";
	}

	public function pass(){
		echo "Passing\n";
	}
}

//类到对象的实例化,类的实例化为对象时使用关键字new，new之后紧跟着类的名称和一对括号
$jordan = new NbaPlayer();
//对象中的属性成员和方法都可以通过->符号来访问
echo $jordan->name."\n";
$jordan->dribble();
echo $jordan->pass();

?>
