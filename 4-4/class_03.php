<?php 
//子类中编写跟父类方法名完全一致的方法可以完成对父类方法的重写（overwrite）
//对于不想被任何类继承的类，可以在class前面添加final关键字，比如如果在这里的父类class前面加上final的话，保存后run一下会提示出错的。
class BaseClass{
	public function test(){
		echo "这个是父类中的test方法\n";
	}
	//对于不想被子类重写的方法，可以在方法定义的前面添加final关键字，比如下面的子类方法中的test1方法取消注释的话，在运行的时候呢会提示出错的。
	final public function test1(){
		echo "这个是父类中的test1方法\n";
	}
}
class ChildClass extends BaseClass{
	public function test($tmp=null){
		echo "这个是子类中的test方法".$tmp."\n";
	}

	//public function test1(){
	//	echo "这个是子类中的test1方法\n";
	//}
}

$obj = new ChildClass();
$obj->test();

?>
