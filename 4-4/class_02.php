<?php 
//子类中编写跟父类方法名完全一致的方法可以完成对父类方法的重写（overwrite）
class BaseClass{
	public function test(){
		echo "这个是父类中的test方法\n";
	}
	
	public function test1(){
		echo "这个是父类中的test1方法\n";
	}
}
class ChildClass extends BaseClass{
	public function test($tmp=null){
		echo "这个是子类中的test方法".$tmp."\n";
	}
}

$obj = new ChildClass();
$obj->test();

?>
