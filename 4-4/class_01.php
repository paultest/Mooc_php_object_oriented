<?php 

class BaseClass{
	public function test(){
		echo "这个是父类中的test方法\n";
	}
	
	public function test1(){
		echo "这个是父类中的test1方法\n";
	}
}
class ChildClass extends BaseClass{
	public function test(){
		echo "这个是子类中的test方法\n";
	}
}

$obj = new ChildClass();
$obj->test();

?>
