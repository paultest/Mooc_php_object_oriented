<?php 
class BaseClass{
	public function test(){
		echo "这个是父类中的test方法\n";
	}
	public function test1(){
		echo "这个是父类中的test1方法\n";
	}
}
class ChildClass extends BaseClass{
	
	const CONST_VALUE = 'A constant value'; //定义常量
	private static $sta_value = "静态变量";
	public function test($tmp=null){
		echo "这个是子类中的test方法".$tmp."\n";
		parent::test(); //用parent关键字可以访问父类中的方法，包括这里被子类重写的方法
		self::aaa();  //可以用self关键字来访问同个类的其他方法，这里也可以用$this->aaa();来实现的。
		echo self::CONST_VALUE."\n";  //也可以用self关键字来访问const属性的值
		echo self::$sta_value."\n"; //这里也可以用static::$sta_value来访问
	}
	public function aaa(){
		echo "这个是子类中的aaa方法\n";
	}
}

$obj = new ChildClass();
$obj->test();

?>
