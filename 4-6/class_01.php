<?php
//interface关键字用于定义接口
interface ICanEat{
	//接口里面的方法不需要有方法的实现
	public function eat($food);
}

//implements关键字用于表示类实现某个接口
class Human implements ICanEat{
	//实现了某个接口之后，必须提供接口中定义的方法的具体实现。
	public function eat($food){
		echo "Human eating ".$food."\n";
	}
}

class Animal implements ICanEat{
	public function eat($food){
		echo "Animal eating ".$food."\n";
	}
}

$obj = new Human();
$obj->eat("Apple");
$monkey = new Animal();
$monkey->eat("Banana");
//不能实例化接口
// $eatObj = new ICanEat();

//可以用instanceof关键字来判断某个对象是否实现了某个接口
var_dump($obj instanceof ICanEat);  //true

//检查对象是否实现了ICanEat接口，是则执行eat方法，否则输出
function checkEat($obj){
	if($obj instanceof ICanEat){
		$obj->eat("food");
	}else{
		echo "该对象没有实现ICanEat接口\n";
	}
}

checkEat($obj);  //检查$obj是否实现了icaneat接口
checkEat($monkey);

//可以用extends让接口继承接口
interface ICanPee extends ICanEat{
	public function pee();
}

//当类实现子接口时，父接口定义的方法也需要在这个类里面具体实现
class Human1 implements ICanPee{
	public function pee(){}
	public function eat($food){}  //必须有这行，由于是继承了ICanEat，所以实现接口的时候也需要把父类的也要实现
}
?>
