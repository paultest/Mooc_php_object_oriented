<?php

class MagicTest{
	//__tostring会在把对象转换为string的时候自动调用
	public function __tostring(){
		return "这个是MagicTest类的tostring方法\n";
	}
	//__invoke会在把对象当做一个方法调用的时候自动调用
	public function __invoke($x){
		echo "调用了MagicTest类的invoke方法，参数为：".$x."\n";
	}
	//方法的重载	
	public function __call($name,$arguments){
		echo "输出MagicTest的call方法，名称为： ".$name.",参数为： ".implode(",",$arguments)."\n";
	}
	//静态方法的重载，注意这个方法要设定为static
	public static function __callStatic($name,$arguments){			
		echo "输出MagicTest的callStatic方法，名称为： ".$name.",参数为： ".implode(",",$arguments)."\n";
	}

	public function aaa(){
		return "这个是MagicTest类的aaa方法\n";
	}

}

$obj = new MagicTest();
echo $obj;
$obj(888);

$obj->runTest();
$obj->runTest2("aaa","bbb","ccc");
MagicTest::runTest("111","222","333");

?>
