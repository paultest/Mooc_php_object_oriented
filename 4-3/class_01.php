<?php

class Human{
	public $name;
	protected  $height;  //只有自身和子类可以访问
	public $weight;
	private $isHungry = true;  //不能被子类访问
	
	public function eat($food){
		echo $this->name."'s eating ".$food."\n";
	}

	public function info(){
		echo "HUMAN: ".$this->name.";".$this->height.";".$this->isHungry."\n";
	}
}

//类的定义以关键字class开始，后面跟着这个类的名称。类的明明通常每个单词的第一个字母大写。以中括号开始和结束
class NbaPlayer extends Human{
	public $team = "Bull";
	public $playerNumber = "23";
	
	private $age = "42";	
	public $president = "David";

	public function change_president($new_president){
		$this->president = $new_president;
	}	

	//构造函数，在对象被实例化的时候自动调用，即下面的$jorder = new NbaPlayer;就调用了构造函数	
	function __construct($name,$height,$weight,$team,$playerNumber){
		echo "In NbaPlayer constructor\n";
		$this->name = $name;  //$this是php里面的伪变量，表示对象自身。可以通过$this->的方式去访问对象的属性和方法。
		$this->height = $height; //父类中的属性，可以通过$this来访问
		$this->weight = $weight;
		$this->team = $team;
		$this->playerNumber = $playerNumber;
	}
	
	//析构函数,在程序执行结束的时候会自动调用,通常被用于清理程序使用的资源。比如程序使用了打印机，那么可以在析构函数里面释放打印机资源。
	function __destruct(){
		echo "Destroying ".$this->name."\n";
	}	

	public function get_age(){
		echo $this->name."'s age is ".($this->age-2)."\n";
	}

}

//类到对象的实例化,类的实例化为对象时使用关键字new，new之后紧跟着类的名称和一对括号
$jordan = new NbaPlayer("Jordan","198cm","98kg","Bull","23");
//对象中的属性成员和方法都可以通过->符号来访问
//echo $jordan->age."\n";  //不能访问age
$jordan->get_age();
$jordan->info();
$jordan->change_president("Alen");
$james = new NbaPlayer("James","111","89kg","Heat","06");
echo $jordan->name.": ".$jordan->president."\n";
echo $james->name.": ".$james->president."\n";


?>
